"""
High-level tests for the  package.

"""

import bpmcap


def test_version():
    assert bpmcap.__version__ is not None
