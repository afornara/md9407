import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import glob
import os
from concurrent import futures
import subprocess
import shutil

destination_path = "/eos/project/l/lhc-lumimod/MD9407"
source_path = "/afs/cern.ch/work/a/aradosla/private/ADTObsBox_datacopy"

days = [17]
hours = [0, 1, 2, 3, 4, 5, 6, 7]

mysymbolic_link = f'sshfs aradosla@cs-ccr-dev1.cern.ch:/nfs/cfc-sr4-adtobs2buf/obsbox/slow ADTObsBox_datacopy -o IdentityFile=/afs/cern.ch/user/a/aradosla/.ssh/id_rsa'
print('Mounting NFS..')
subprocess.call(f"{mysymbolic_link}", shell=True)
#quit()
'''
def copy_file(file_path):
    dest_dir = os.path.dirname(file_path).replace(source_path, destination_path)
    os.makedirs(dest_dir, exist_ok=True)
    dest_file_path = os.path.join(dest_dir, os.path.basename(file_path))
    print(dest_file_path)
    if not os.path.exists(dest_file_path):
        shutil.copy2(file_path, dest_file_path)
        print(f"Saving to: {dest_file_path}")
    else:
        print(f"Already saved: {dest_file_path}")
'''
def copy_files(file_path):

      dir_path = os.path.dirname(file_path)

      dest_subdir = os.path.join(destination_path, dir_path[len(source_path)+1:])
      os.makedirs(dest_subdir, exist_ok=True)
      full_tosave = (os.path.join(dest_subdir, file_path.split("/")[-1]))
      full_from = file_path
      if not os.path.exists(full_tosave):
        shutil.copy2(full_from, full_tosave)
        print(f"Saving to: {full_tosave}")
      else:
        print(f"Already saved: {full_tosave}")




def process_beamplane(beamplane):
    for day in days:
        for hour in hours:  # Loop over the hours
            print(beamplane, day, hour)
            files = glob.glob(f'{source_path}/{beamplane}/{day}/{hour}/*.h5')
            print(files)
            for file_path in sorted(files):
                copy_files(file_path)


beamplanes = ["B1H_Q7", "B2H_Q7", "B1V_Q7", "B2V_Q7", "B1H_Q8", "B2H_Q8", "B1V_Q8", "B2V_Q8",
              "B1H_Q9", "B2H_Q9", "B1V_Q9", "B2V_Q9"]

with futures.ThreadPoolExecutor() as executor:
    print('now saving')
    executor.map(process_beamplane, beamplanes)

