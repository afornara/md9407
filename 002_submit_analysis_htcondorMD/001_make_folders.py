import pandas as pd
import tree_maker
from tree_maker import NodeJob
from tree_maker import initialize
import time
import os
from pathlib import Path
import itertools
import numpy as np
import yaml
from user_defined_functions import generate_run_sh
from user_defined_functions import generate_run_sh_htc
from pathlib import Path
import shutil

config = yaml.safe_load(open('config.yaml'))

#bunch_nb_b1 = ['all']
#bunch_nb_b2 = ['all']



######## CHANGE THE BUNCHES #########
bunch_nb_b1 = [0, 320, 620, 960, 1280, 1600, 1920, 2240, 2560, 2880]
#bunch_nb_b1 = [21, 341, 661, 981, 1301, 1621, 1941, 2261, 2581, 2901]
#bunch_nb_b1 = [42, 362, 682, 1002, 1322, 1642, 1962, 2282, 2602, 2922]
#bunch_nb_b1 = [63, 383, 703, 1023, 1343, 1663, 1983, 2303, 2623, 2943]
bunch_tot_num = len(bunch_nb_b1)

bunch_nb_b2 = [0, 320, 620, 960, 1280, 1600, 1920, 2240, 2560, 2880]
#bunch_nb_b2 = [21, 341, 661, 981, 1301, 1621, 1941, 2261, 2581, 2901]
#bunch_nb_b2 = [42, 362, 682, 1002, 1322, 1642, 1962, 2282, 2602, 2922]
#bunch_nb_b2 = [63, 383, 703, 1023, 1343, 1663, 1983, 2303, 2623, 2943]





path = 'ADTObsBox_data'
fillnb = 8957
#position = 'all'
position = 1
save_to_base = "/eos/user/a/aradosla/FFTs"  # Base directory to store the results

file_list =  [
    f"filenames_Fill{fillnb}_B1H_Q7_{position}.parquet",
    f"filenames_Fill{fillnb}_B1V_Q7_{position}.parquet",
    f"filenames_Fill{fillnb}_B2H_Q7_{position}.parquet",
    f"filenames_Fill{fillnb}_B2V_Q7_{position}.parquet",
    f"filenames_Fill{fillnb}_B1H_Q8_{position}.parquet",
    f"filenames_Fill{fillnb}_B1V_Q8_{position}.parquet",
    f"filenames_Fill{fillnb}_B2H_Q8_{position}.parquet",
    f"filenames_Fill{fillnb}_B2V_Q8_{position}.parquet",
    f"filenames_Fill{fillnb}_B1H_Q9_{position}.parquet",
    f"filenames_Fill{fillnb}_B1V_Q9_{position}.parquet",
    f"filenames_Fill{fillnb}_B2H_Q9_{position}.parquet",
    f"filenames_Fill{fillnb}_B2V_Q9_{position}.parquet"
]

for file_name in file_list:
    # Load the filenames from the current file
    filenames = pd.read_parquet(file_name).filenames.values

    # Extract the beamplanes and pickups from the filenames
    beamplanes = set([filename.split("/")[1] for filename in filenames])
    pickups = set([filename.split("/")[2] for filename in filenames])

    for beamplane in beamplanes:
        for pickup in pickups:
            study_name = f"Fill{fillnb}_{beamplane}_{pickup}_{position}"
            save_to = f"{save_to_base}/{study_name}"
            Path(save_to).mkdir(parents=True, exist_ok=True)

            mypython = "/afs/cern.ch/work/a/aradosla/private/miniforge3/bin/activate"
            n = 2
            filenames_filtered = [filename for filename in filenames if beamplane in filename and pickup in filename]
            filenames_in_chunks = [filenames_filtered[i:i + n] for i in range(0, len(filenames_filtered), n)]

            children = {}
            for i, chunk in enumerate(filenames_in_chunks):
                child_name = f"{study_name}/{i:03}"
                child_save = f"{study_name}_{i:03}"
                children[child_name] = {
                    'adt_file': chunk,
                    'save_to': f"{save_to}/results_fft_{child_save}.parquet",
                    'bunch_nb_b1': bunch_nb_b1,
                    'bunch_nb_b2': bunch_nb_b2,
                    'log_file': f"{os.getcwd()}/{child_name}/tree_maker.log"
                }

            config['root']['children'] = children
            config['root']['setup_env_script'] = mypython
            config['root']['symbolic_link'] = f"sshfs aradosla@cs-ccr-dev1.cern.ch:/nfs/cfc-sr4-adtobs2buf/obsbox/slow {path} -o IdentityFile=/afs/cern.ch/user/a/aradosla/.ssh/id_rsa"
            config['root']['make_path'] = 'ADTObsBox_data'

            # Create tree object
            start_time = time.time()
            root = initialize(config)
            print('Done with the tree creation.')
            print("--- %s seconds ---" % (time.time() - start_time))

            # Move to the file system
            start_time = time.time()
            root.make_folders(generate_run_sh_htc)
            print('The tree folders are ready.')
            print("--- %s seconds ---" % (time.time() - start_time))

            shutil.move("tree_maker.json", f"tree_maker_{study_name}.json")
            shutil.move("tree_maker.log", f"tree_maker_{study_name}.log")

