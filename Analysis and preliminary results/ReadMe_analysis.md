# Frequency analysis (8 kHz)for the different gains on Beam 1 and Beam 2, horizontal H and vertical V



Plot4_bunch1 - 4 plots for the first gain, analogous for bunch2, bunch3 and bunch4 

Gain:
2nd Fill, from 20230617_05h37m00s000000us to 20230617_06h25m00s000000us:
 - Loop 1: 15 turns -  Bunch 1 (every first bunch from the batch is selected)           strongest gain
 - Loop 2: 27 turns - Bunch 2 (every second bunch from the batch is selected) 
 - Loop 3: 47 turns - Bunch 3 (every third bunch from the batch is selected) 
 - Loop 4: 931 turns - Bunch 4 (every forth bunch from the batch is selected)        weakest gain


